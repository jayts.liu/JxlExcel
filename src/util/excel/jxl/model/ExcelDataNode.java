/* Copyright (C) 2017 Jay Liu to Present. - All Rights Reserved. */
package util.excel.jxl.model;

import java.util.List;

/**
 * 
 * @author Jay Liu
 * @date 20171019
 * @version 0.1
 */
public class ExcelDataNode {

	private String sheetName;
	private String[][] titleList;
	private String[] columnList;
	private List<?> dataList;
	
	private Integer contentLength;

	public int getContentLength() {
		if(contentLength == null) 
			return titleList[titleList.length].length; //last row length
		
		return contentLength;
	}
	
	public void setContentLength(Integer contentLength) {
		this.contentLength = contentLength;
	}

	public String getSheetName() {
		return sheetName;
	}

	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}

	public String[][] getTitleList() {
		return titleList;
	}

	public void setTitleList(String[][] titleList) {
		this.titleList = titleList;
	}

	public List<?> getDataList() {
		return dataList;
	}

	public void setDataList(List<?> dataList) {
		this.dataList = dataList;
	}

	public String[] getColumnList() {
		return columnList;
	}

	public void setColumnList(String[] columnList) {
		this.columnList = columnList;
	}

}
