/* Copyright (C) 2017 Jay Liu to Present. - All Rights Reserved. */
package util.excel.jxl;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.List;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import util.excel.jxl.model.ExcelDataNode;


/**
 * for jxl Excel
 * Copyright (C) Jay Liu - All Rights Reserved
 * @date 20171019
 * @version 0.1
 */
public class ExcelGenerator {

	WritableWorkbook writableWorkbook;
	
	public ExcelGenerator() {}
	
	public void initExcelData(List<ExcelDataNode> nodeList, String url) throws Exception {
		writableWorkbook = Workbook.createWorkbook(new File(url));
		
		for(ExcelDataNode node : nodeList) {
			WritableSheet sheet = initSheet(node.getSheetName());
			initSheetTitle(sheet, node.getTitleList()); //prepare sheet

			initSheetContent(sheet, node); //to set data
		}
		
	}
	
	//TODO config by xml
	//TODO convert directly by database result set || result set stream
	 
/** private method **/
	private WritableSheet initSheet(String sheetName) {
		return writableWorkbook.createSheet(sheetName, writableWorkbook.getNumberOfSheets()); //add
	}
	
	private WritableSheet initSheetTitle(WritableSheet sheet, String[][] titleList) throws Exception { 
		
		for (int r = 0; r < titleList.length; r++) { //add title row
			for(int c = 0; c < titleList[r].length; c++) { //column
				Label label = new Label(c, r, titleList[r][c]);
				sheet.addCell(label);
			}
		}
		return sheet;
	}
	
	private WritableSheet initSheetContent(WritableSheet sheet, ExcelDataNode node) throws Exception {
		
		String[][] titleList = node.getTitleList();
		String[] columnList =(node.getColumnList() != null ? node.getColumnList() : titleList[titleList.length-1]);
		int tLastRow = titleList.length; //get title length
	
		
		for (int r = 0; r < node.getDataList().size(); r++) { //add data
			Object obj = node.getDataList().get(r);

			for (int c = 0; c < columnList.length; c++) {
				String column = columnList[c];
				Field field = obj.getClass().getDeclaredField(column);
				Type type = field.getType();
				String def = (type == boolean.class || type == Boolean.class) ? "is" : "get";
				
				Method method = obj.getClass().getDeclaredMethod(def + column.substring(0,1).toUpperCase() + column.substring(1));
				Object value = method.invoke(obj);
				
				Label label = new Label(c, r+tLastRow, String.valueOf(value));
				sheet.addCell(label);
			}
		}
		
		return sheet;
	}
	
	public boolean toExcelFile() {
		try {
			writableWorkbook.write();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				writableWorkbook.close();
			} catch (WriteException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

}

