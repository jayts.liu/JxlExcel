package test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import util.excel.jxl.ExcelGenerator;
import util.excel.jxl.model.ExcelDataNode;
import util.parser.SimpleObjectParser;



/**
 * 
 * @author Jay Liu
 * @date 2017年10月19日
 * @version 0.1
 */
public class TestExcelGenerator {

	public static void main(String[] args) throws Exception {
		
		Customer cus = new Customer();
		SimpleObjectParser.put(cus, "id", "123");
		SimpleObjectParser.put(cus, "cus_name", "王老先生");
		SimpleObjectParser.put(cus, "price", new BigDecimal(100.01));
		SimpleObjectParser.put(cus, "amount", 33);
		System.out.println(cus.toString());
		
		ExcelGenerator gen = new ExcelGenerator();
		List<ExcelDataNode> nodeList = new ArrayList<ExcelDataNode>();
		ExcelDataNode node = new ExcelDataNode();
		
		List cusList = new ArrayList(); cusList.add(cus); cusList.add(cus);//add two obj
		
		node.setSheetName("shee1");
		
		String titleList[][] = {
				{"ID", "CUS_CODE", "CUS_NAME", "HADDRESS", "PRICE", "AMOUNT"}
				,{"1", "2", "3", "4", "5", "6"}
		};
		node.setTitleList(titleList);
		node.setDataList(cusList);
		
		String columnList[] = {"id", "cus_code", "cus_name", "haddress", "price", "amount"};
		node.setColumnList(columnList);
		
		nodeList.add(node);
		gen.initExcelData(nodeList, "doc/test.xls");
		gen.toExcelFile();
	}

}
