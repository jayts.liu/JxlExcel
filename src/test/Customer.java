package test;

import java.lang.reflect.Field;
import java.math.BigDecimal;

public class Customer {
	
	private String id, cus_code, cus_name, haddress;
	private BigDecimal price;
	private int amount;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCus_code() {
		return cus_code;
	}

	public void setCus_code(String cus_code) {
		this.cus_code = cus_code;
	}

	public String getCus_name() {
		return cus_name;
	}

	public void setCus_name(String cus_name) {
		this.cus_name = cus_name;
	}

	public String getHaddress() {
		return haddress;
	}

	public void setHaddress(String haddress) {
		this.haddress = haddress;
	}
	
	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
	    StringBuilder sb = new StringBuilder();

	    Class<?> thisClass = null;
	    try {
	        thisClass = Class.forName(this.getClass().getName());

	        Field[] aClassFields = thisClass.getDeclaredFields();
	        sb.append(this.getClass().getSimpleName() + " [ ");
	        for(Field f : aClassFields){
	            String fName = f.getName();
	            sb.append("(" + f.getType() + ") " + fName + " = " + f.get(this) + ", ");
	        }
	        sb.append("]");
	    } catch (Exception e) {
	        e.printStackTrace();
	    }

	    return sb.toString();
	}

}
